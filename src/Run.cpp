//============================================================================
// Name        : FPU_verification.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>

/**
 * dataSize in bytes! Use the sizeof()!
 *
 */
void printNativeAndIEEEBinary(void* data, uint8_t dataSize, FILE *fp) {
	printf("nat binary=");
	switch (dataSize) {
	case 4: {
		uint32_t inputBin = *(uint32_t*) data;
		for (uint8_t i = 0; i < (dataSize * 8); ++i) {
			printf("%i", (inputBin >> ((dataSize * 8) - 1 - i)) & 0b1);
		}
		// setting to file
		for (uint8_t i = 0; i < dataSize; ++i) {
			fprintf(fp, "%c",
					(uint8_t) ((inputBin >> ((dataSize - 1 - i) * 8))
							& 0b11111111u));
//			printf("\n");
//			printf("%i", ((inputBin >> ((dataSize - 1 - i) * 8)) & 0b11111111u));
		}
		break;
	}
	default: {
		printf("Unsupported size yet");
	}
		break;
	}
	printf("\n");

}

int main() {
	FILE *fp;
	fp = fopen("test.bin", "wb+");
	srand(time(NULL));

	//////////////////////
	float input = 1.0f;
	printf("input=%f\n", input);
	printNativeAndIEEEBinary(&input, sizeof(input), fp);
	//////////////////////
	float input2 = -1.0f;
	printf("input=%f\n", input2);
	printNativeAndIEEEBinary(&input2, sizeof(input2), fp);
	//////////////////////
	float inputp1 = input * input2;
	printf("input=%f\n", inputp1);
	printNativeAndIEEEBinary(&inputp1, sizeof(inputp1), fp);
	//////////////////////
	float inputa = 2.0f;
	printf("input=%f\n", inputa);
	printNativeAndIEEEBinary(&inputa, sizeof(inputa), fp);
	//////////////////////
	float inputb = -2.0f;
	printf("input=%f\n", inputb);
	printNativeAndIEEEBinary(&inputb, sizeof(inputb), fp);
	//////////////////////
	float inputp2 = inputa * inputb;
	printf("input=%f\n", inputp2);
	printNativeAndIEEEBinary(&inputp2, sizeof(inputp2), fp);
	//////////////////////
	float inputc = INFINITY;
	printf("input=%f\n", inputc);
	printNativeAndIEEEBinary(&inputc, sizeof(inputc), fp);
	//////////////////////
	float inputd = -INFINITY;
	printf("input=%f\n", inputd);
	printNativeAndIEEEBinary(&inputd, sizeof(inputd), fp);
	//////////////////////
	float inputp3 = inputc * inputd;
	printf("input=%f\n", inputp3);
	printNativeAndIEEEBinary(&inputp3, sizeof(inputp3), fp);
	//////////////////////
	float input3 = -64.2f;
	printf("input=%f\n", input3);
	printNativeAndIEEEBinary(&input3, sizeof(input3), fp);
	/////////////////////
	float input4 = 64.2f;
	printf("input=%f\n", input4);
	printNativeAndIEEEBinary(&input4, sizeof(input4), fp);
	/////////////////////
	float inputp = input3 * input4;
	printf("input=%f\n", inputp);
	printNativeAndIEEEBinary(&inputp, sizeof(inputp), fp);
	//////////////////////
	float input5 = +0.0f;
	printf("input=%f\n", input5);
	printNativeAndIEEEBinary(&input5, sizeof(input5), fp);
	/////////////////////
	float input6 = -0.0f;
	printf("input=%f\n", input6);
	printNativeAndIEEEBinary(&input6, sizeof(input6), fp);
	/////////////////////
	float inputp6 = input5 * input6;
	printf("input=%f\n", inputp6);
	printNativeAndIEEEBinary(&inputp6, sizeof(inputp6), fp);
	//////////////////////
	float input7 = -NAN;
	printf("input=%f\n", input7);
	printNativeAndIEEEBinary(&input7, sizeof(input7), fp);
	/////////////////////
	float input8 = NAN;
	printf("input=%f\n", input8);
	printNativeAndIEEEBinary(&input8, sizeof(input8), fp);
	/////////////////////
	float inputp9 = input7 * input8;
	printf("input=%f\n", inputp9);
	printNativeAndIEEEBinary(&inputp9, sizeof(inputp9), fp);

	for (int i = 0; i < 3995; ++i) {
		float inputz1 = (float)rand()/ (float)rand();
		printf("input1=%f\n", inputz1);
		printNativeAndIEEEBinary(&inputz1, sizeof(inputz1), fp);
		/////////////////////
		float inputz2 =(float)rand()/ (float)rand();
		printf("input2=%f\n", inputz2);
		printNativeAndIEEEBinary(&inputz2, sizeof(inputz2), fp);
		/////////////////////
		float inputzp = inputz1 * inputz2;
		printf("inputp=%f\n", inputzp);
		printNativeAndIEEEBinary(&inputzp, sizeof(inputzp), fp);

	}

	fclose(fp);
	return 0;
}
